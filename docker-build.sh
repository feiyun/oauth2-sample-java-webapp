#!/bin/bash

timestamp="latest"

echo "Building CAS docker image tagged as [$timestamp]"
# read -p "Press [Enter] to continue..." any_key;

docker build --tag="registry.cn-shanghai.aliyuncs.com/byton-k8s-test/httpstest:$timestamp" . \
  && echo "Built CAS image successfully tagged as registry.cn-shanghai.aliyuncs.com/byton-k8s-test/httpstest:$timestamp" \
  && docker images "registry.cn-shanghai.aliyuncs.com/byton-k8s-test/httpstest:$timestamp"