FROM openjdk:8u111-jdk-alpine
VOLUME /tmp
RUN cd / && mkdir -p /etc/cas
COPY thekeystore /etc/cas/
ADD /target/sso-0.0.1-SNAPSHOT.jar app.jar
ENTRYPOINT ["java","-Dspring.profiles.active=cas -Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]