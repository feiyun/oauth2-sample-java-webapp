#!/bin/bash

docker stop sso > /dev/null 2>&1
docker rm sso > /dev/null 2>&1
docker run -d -p 9990:9999 --name="sso" registry.cn-shanghai.aliyuncs.com/byton-k8s-test/httpstest:latest
docker logs -f sso