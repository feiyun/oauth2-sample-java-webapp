package demo;

import com.google.common.base.Charsets;
import com.google.common.io.Files;
import com.google.gson.Gson;
import kong.unirest.HttpResponse;
import kong.unirest.Unirest;
import lombok.val;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.Hex;
import org.jose4j.jwe.JsonWebEncryption;
import org.jose4j.jwk.JsonWebKey;
import org.jose4j.jwk.JsonWebKeySet;
import org.jose4j.jwk.PublicJsonWebKey;
import org.jose4j.jwk.RsaJsonWebKey;
import org.jose4j.jws.JsonWebSignature;
import org.jose4j.jwt.JwtClaims;
import org.jose4j.keys.AesKey;
import org.junit.Test;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.security.Key;
import java.security.MessageDigest;
import java.security.SecureRandom;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


public class OpenIDTests {
    private final static String OAUTH_SERVER_HOST = "https://cas.dev.byton.cn";
    //private final static String OAUTH_SERVER_HOST = "https://dev.liftme.club:8443";

    @Test
    public void generateLoginUrl() throws Exception {
        String verifier = getVerifier();
        String challenge = generateChallenge("S256", verifier);

        System.out.println("challenge: " + challenge);

        String url = String.format("%s/cas/oidc/authorize?" +
                "client_id=oidcclient&" +
                "redirect_uri=https://localhost:8888/dashboard/login&" +
                "response_type=code&" +
                "code_challenge=%s&bypass_approval_prompt=true&" +
                "code_challenge_method=S256&scope=openid+profile+offline_access", OAUTH_SERVER_HOST, challenge);
        String url2 = String.format("https://www.liftme.club:8443/cas/oauth2.0/authorize?" +
                "client_id=exampleOauthClient&" +
                "redirect_uri=https://localhost:8888&" +
                "response_type=code");
        System.out.println(url);
    }

    @Test
    public void getAccessCodeThruPkce() throws Exception {
        String body = String.format("grant_type=authorization_code&client_id=oidcclient" +
                "&client_secret=oidcbytonDeCloud-2020&redirect_uri=https://localhost:8888" +
                "&code=OC-1-KUbfwhprvFNZbLxygWyLrh6TLYAfewNQ" + "&code_verifier=Cws_ERVnQAoh4uKHE-ocDe7arg4plDd7NHl6EgDdhrU");
        HttpResponse<String> response = Unirest.post(OAUTH_SERVER_HOST+"/cas/oidc/token")
                .header("content-type", "application/x-www-form-urlencoded")
                .body(body)
                .asString();
        System.out.println("response:" + response.getBody() + " \nstatus: " + response.getStatus());
        if (response.getStatus() == 200) {
            String responseBodybody =  response.getBody();
            Gson gson = new Gson();
            Map bodyMap = gson.fromJson(responseBodybody, Map.class);
            String access_token = (String) bodyMap.get("access_token");
            System.out.println(String.format("%s/cas/oidc/profile?access_token=%s", OAUTH_SERVER_HOST, access_token));
            String id_token = (String) bodyMap.get("id_token");
            System.out.println(String.format("id_token: %s", id_token));
            decodeJWT(id_token);
        }
    }

    @Test
    public void getAccessCodeThruRresourceOwnerPassword() throws Exception {
        String body = "grant_type=password&client_id=oidcclient" +
                "&client_secret=oidcbytonDeCloud-2020&username=%2B8613501095275&password=1qaz2wsx";
        //import kong.unirest.Unirest;
        HttpResponse<String> response = Unirest.post(OAUTH_SERVER_HOST+"/cas/oidc/token")
                .header("content-type", "application/x-www-form-urlencoded")
                .body(body)
                .asString();
        System.out.println("response:" + response.getBody() + " \nstatus: " + response.getStatus());
        if (response.getStatus() == 200) {
           String responseBodybody =  response.getBody();
                    Gson gson = new Gson();
            Map bodyMap = gson.fromJson(responseBodybody, Map.class);
            String access_token = (String) bodyMap.get("access_token");
            System.out.println(String.format("%s/cas/oauth2.0/profile?access_token=%s", OAUTH_SERVER_HOST, access_token));
            String id_token = (String) bodyMap.get("id_token");
            decodeJWT(id_token);
        }
    }

    @Test public void fetchRefreshToken() throws Exception {
        String body = String.format("grant_type=refresh_token&client_id=oidcclient" +
                "&client_secret=oidcbytonDeCloud-2020&refresh_token=RT-46-W0-CBlNhw-1okmtGudt-zZmNsb7rV-7L");
        //import kong.unirest.Unirest;
        HttpResponse<String> response = Unirest.post(OAUTH_SERVER_HOST + "/cas/oidc/accessToken")
                .header("content-type", "application/x-www-form-urlencoded")
                .body(body)
                .asString();
        System.out.println("response:" + response.getBody() + " \nstatus: " + response.getStatus());
        if (response.getStatus() == 200) {
            String responseBodybody =  response.getBody();
            Gson gson = new Gson();
            Map bodyMap = gson.fromJson(responseBodybody, Map.class);
            String access_token = (String) bodyMap.get("id_token");
            System.out.println(String.format("%s/cas/oauth2.0/profile?access_token=%s", OAUTH_SERVER_HOST, access_token));

            decodeJWT(access_token);
        }
    }


    @Test
    public void getAccessCodeThruDeviceFlow() throws Exception {
        String body = "response_type=device_code&client_id=oidcclient&scope=openid+profile+offline_access";
        //import kong.unirest.Unirest;
        HttpResponse<String> response = Unirest.post(OAUTH_SERVER_HOST+"/cas/oidc/token")
                .header("content-type", "application/x-www-form-urlencoded")
                .body(body)
                .asString();
        System.out.println("response:" + response.getBody() + " \nstatus: " + response.getStatus());
        if (response.getStatus() == 200) {
            String responseBodybody =  response.getBody();
            Gson gson = new Gson();
            Map bodyMap = gson.fromJson(responseBodybody, Map.class);
            String verification_uri = (String) bodyMap.get("verification_uri");
            System.out.println("verification_uri: " + verification_uri);

        }
    }

    @Test
    public void getAccessTokenFromDevice() throws Exception {
        String body = "response_type=device_code&client_id=oidcclient&code=ODT-9-GRBB4BnMQDGfC5tjY00l8TvHROheVrmU";
        //import kong.unirest.Unirest;
        HttpResponse<String> response = Unirest.post(OAUTH_SERVER_HOST+"/cas/oauth2.0/accessToken")
                .header("content-type", "application/x-www-form-urlencoded")
                .body(body)
                .asString();
        System.out.println("response:" + response.getBody() + " \nstatus: " + response.getStatus());
        if (response.getStatus() == 200) {
            String responseBodybody =  response.getBody();
            Gson gson = new Gson();
            Map bodyMap = gson.fromJson(responseBodybody, Map.class);
            String verification_uri = (String) bodyMap.get("verification_uri");
            System.out.println("verification_uri: " + verification_uri);

        }
    }

    @Test
    public void jwtTest() throws  Exception {
        final String signingKey = "FSGeyZDp7VgPWJKVAS7sF-FIbfQ0822wkQuF48yrT1VlBTI290scULQr9RB2Zd0UJZuf-T061saJXokqvDROOg";
        final String encryptionKey = "osFdnwrVA3lIoARpUX6CFGjspjjkFmE0Cze7bVZTfio";

        //String secureJwt = "eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.ZXlKNmFYQWlPaUpFUlVZaUxDSmhiR2NpT2lKa2FYSWlMQ0psYm1NaU9pSkJNVEk0UTBKRExVaFRNalUySWl3aVkzUjVJam9pU2xkVUlpd2lkSGx3SWpvaVNsZFVJbjAuLkRCLWxONHVDZ3NCaWpzVk5zOWp6VmcuakRzYkMzWUVOS3puSU8zbnVCOWVDUlNSd2VfYlV4VFZlQXpPU3JrNHRJVU1BRmFOVlFUUlc1Mkx2RkVJX2xtc1hBWGNLaGxraGhpdklua3VBVGRIVS1CXzR1NU8yZ2d6NHB6eEktNE1yenNRVk93dmRnRXhUVVdKWGhxbFVEVXF0aTNWaFUxbXFGOGQ1R0JXVXVnTW9lWEsxNGNIX3lBNTIwQUJpU3U0dmlxLTBXTXVKSzhfNDN5WHJrZldLdGMzblNsNUFaOGRNdFRWU0dxQUUxaDVDeGVZZWdkRFlfbFo4NWQ0ZjNZVEtaT2JCWnF4R0JZYVhwTWFRSmJRam04aW01ZVI2UVFvOVR3cTNZYllDWFFzVk5UZ0NKQ0tWZFdLOUQ5TDhDWHBaOWcucUN0Z19MRFZDY2dqcS04UHF4TGgxUQ.EciBHuVcaH5JPyG116LB714ADiu-CYyz8ccZVb_dmpkZQar-GxvIWgQ57kfkvzMtl-D_YdeKyN1Zz34TvaCRgg";
        //String sjwt = "eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.ZXlKNmFYQWlPaUpFUlVZaUxDSmhiR2NpT2lKa2FYSWlMQ0psYm1NaU9pSkJNVEk0UTBKRExVaFRNalUySWl3aVkzUjVJam9pU2xkVUlpd2lkSGx3SWpvaVNsZFVJbjAuLmt3LWI3T1M1cjJkQzVQOUd4TlI2UlEuX0lWZkJqSzNDc1FtWGhjR0NOQnRLaXZtcU9aYjd3RV9LeDd5ZzZUN2pISmxKcjAzdGJfSG1sTC14RTRsc0FNSEkybklWZWJjdThPck5TTEVQRzkzT0RXY0hNOFJFTUxNVXdVZThoWHVlRUFvOUR4VHhRWlNGbnl1ZlQtdXozRncxWFZ0R0dQXzdlQUt3WXBfT2dkRjNUMHZhZjQ3a09jUWNraTlaZjN0bUMzMkp1X0FkckliZEhNTlVxbnAzT2tvSGRtUmRQbWlha0hYSWhDcHBTU0NEU1lYNm1yaFlETFpVaFFjZmc0dThvYzNPVEJtSWszNXJJNEFyc3ZROTJsS25sVjd4ai1VU3h0cWsxNFZ5dUNfNFEwSWVCdVhmdmRfX3NIZ05ZcndoR1kuckhwZ1dpVHVkN3JOZm4xUEQ2azhTdw.wec3WYkS-J91WmIEEk1EJXWXJ0DopHPByg_21AHpUHQZUVquK_5BVLest1QS3jBbvs-EAzOXRQMCTsDohSAxqw";
        String jwt = "eyJhbGciOiJSUzUxMiIsInR5cCI6IkpXVCJ9.ZXlKNmFYQWlPaUpFUlVZaUxDSmhiR2NpT2lKa2FYSWlMQ0psYm1NaU9pSkJNVEk0UTBKRExVaFRNalUySWl3aVkzUjVJam9pU2xkVUlpd2lkSGx3SWpvaVNsZFVJbjAuLkoya1ZCNEQ5MjRUQjhHTVhCVkRUQ0EuNDMtM0JXakhRckgxYTM4TFdiekE0ZW03WG9seFJ0dUNVRGlQXzZaVFpaMlZzN0hrcDhFQjY2cGZwNHpQeGZ3aHhraDU1dXNIQzY2SGJPNXJxcEVuczlFMmZyRWY1TW5vbHpQZjZZeU9VN0hjTk9kdm9NcUJWRnpXNmM4bV9acTNZRldZM1llcm9CdjlsX2o0S2U4d2dZd2xkUnVVQmVIRW53ZXlWUkloUGZRMmtHSTVVVnljeldpeVpBZ1pDV0h4d3VNYS04SHl0WlpNUmNMYmdpRmV1WGxmRDd5UG9wVWc5eVE2dERKNE5DY3I4ZzRJc3FPcmUxR0FfVFhnWW9rWldJVjctVDRuNEJfdWItdVg2WENlbE4tLW9WcnFhMHFLc2Uyck1QSUhzOXFvbV9QaFJqRE0zdzRYdnNoQldGTUh2ZFl5OVhISzRUeUR2ZWVldTRjanBnLldzQVV0cjBmdGtFNVlEbU0zYmg3WHc.kzLbcZIQ2fh73w2OEO7JBGlz-ARlFLPbjwNxNcyBA9xS0WbQladTchqs8JC05YhSK882brMChB8TdYPLshdKBXc-pUd3-ckwRY99qnqkfzUbqtCAZPnf-iHAqPGG2qldM6tvKZg3T_zOwk3A30rKHwkhQFDepIH8kL54wIMHOBN65miQkS2AYo8TlUxXH46RSwp2nByrBBM1HEVbc94AJO3w1TLONglAnGLB_RAE0DIriwCrYHakw13v-8nIVk4g_CuzvBWlW0ReUmEuWlxxRKMrzttF-88XIDUbrfvpLeiwioK4P0YlpNDusq1HW9AWNmM8wo6P8J9qxsLR9iUGqA";
        final Key key = new AesKey(signingKey.getBytes(StandardCharsets.UTF_8));

        final JsonWebSignature jws = new JsonWebSignature();
        jws.setCompactSerialization(jwt);
        jws.setKey(key);
        if (!jws.verifySignature()) {
            throw new Exception("JWT verification failed");
        }

        final byte[] decodedBytes = Base64.decodeBase64(jws.getEncodedPayload().getBytes(StandardCharsets.UTF_8));
        final String decodedPayload = new String(decodedBytes, StandardCharsets.UTF_8);

        final JsonWebEncryption jwe = new JsonWebEncryption();
        final JsonWebKey jsonWebKey = JsonWebKey.Factory
                .newJwk("\n" + "{\"kty\":\"oct\",\n" + " \"k\":\"" + encryptionKey + "\"\n" + "}");

        jwe.setCompactSerialization(decodedPayload);
        jwe.setKey(new AesKey(jsonWebKey.getKey().getEncoded()));
        System.out.println(jwe.getPlaintextString());
    }
    private String generateChallenge(final String method, final String codeVerifier) {
        if ("plain".equalsIgnoreCase(method)) {
            return codeVerifier;
        }
        if ("S256".equalsIgnoreCase(method)) {
            byte[] bytes = codeVerifier.getBytes(StandardCharsets.UTF_8);
            try {
                MessageDigest md = MessageDigest.getInstance("SHA-256");
                md.update(bytes, 0, bytes.length);
                byte[] digest = md.digest();
                char[] result = Hex.encodeHex(digest);
                String sha256 = new String(result);
                //import org.apache.commons.codec.binary.Base64;
                return Base64.encodeBase64URLSafeString(sha256.getBytes(StandardCharsets.UTF_8));
            } catch (final Exception cause) {
                throw new SecurityException(cause);
            }
        }
        return null;
    }

    private String getVerifier() {
        SecureRandom sr = new SecureRandom();
        byte[] code = new byte[32];
        sr.nextBytes(code);
        //import org.apache.commons.codec.binary.Base64;
        String verifier =  Base64.encodeBase64URLSafeString(code);
        System.out.println("verifier: " + verifier);
        return verifier;
    }

    @Test
    public void decodeTest() throws Exception {
        String jwt = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiJUR1QtMy1HUk1EU1dvYmo4dDEwTHZ2UGUyZVdTVmROc1hET1lIR1BHbFVMN3VZQmVzMmY5U0VIOWlRS0hta212OEx5eXhQWWUwLUNOTFcwMjE4IiwiaXNzIjoiaHR0cDovL2xvY2FsaG9zdDo4MDgwL2Nhcy9vaWRjIiwiYXVkIjoib2lkY2NsaWVudCIsImV4cCI6MTU3NzE1ODY4OCwiaWF0IjoxNTc3MTU4NjU4LCJuYmYiOjE1NzcxNTgzNTgsInN1YiI6ImI1YWI5Y2M4LTllZWItNDc3Ni1iNzU2LTg0OTJjNTQzMWFkNyIsImNsaWVudF9pZCI6Im9pZGNjbGllbnQiLCJzdGF0ZSI6IiIsIm5vbmNlIjoiIiwiYXRfaGFzaCI6Il92NllQdnIxVWZ6ejJYUEE1MXF4TVEiLCJwcmVmZXJyZWRfdXNlcm5hbWUiOiJiNWFiOWNjOC05ZWViLTQ3NzYtYjc1Ni04NDkyYzU0MzFhZDcifQ.sxQO_EjL_svmxzLPqGdK39uYpsI-JjA-b7lVGT_ITi3OLugYZay-vDEzO2_p2FViDU3nZ4auEPe_lDog_zDrIm6Np3b-mGNV3nwluOq5n17u9eDASnluwAZFTCY-sqXmkwBWoBE7eo3QrP8OgMJTHdgCOyx2YaqyMGP_BdrykxYURErwJlyzmocdxnVqoI3OSAWmK7aRUl8UyC-23OeJr1rm7tQWqPKfs3ZqnkDbBy5oW2Rvbm2KiFB1aawNX3xGqAnRcY2j1JLTkv5cSfHyFOTvN04mtphtSppcRHAMQX8jnWWfx5Dss_l9to9jqffxzbCFef5PglP-nQcGmXiSKw";
        decodeJWT(jwt);
    }

    private void decodeJWT(String jwt) throws Exception {
        //String jwt = "eyJhbGciOiJSUzUxMiIsInR5cCI6IkpXVCJ9.ZXlKNmFYQWlPaUpFUlVZaUxDSmhiR2NpT2lKa2FYSWlMQ0psYm1NaU9pSkJNVEk0UTBKRExVaFRNalUySWl3aVkzUjVJam9pU2xkVUlpd2lkSGx3SWpvaVNsZFVJbjAuLnFrY0ZyUXpzNHdyZTZxZmI5Q3hqWHcudldrVkFWc0NHMktxdjdGdlR5dlQzV000Z2FxU2F4VVdKVDVwTmN2T19VYmh2cFkyYUVfSkQwVzVRUmhncnVYQ1cxblQ0MXZWSXFDX3llbmx1NFlXbjRiNGhQMkpQT2NiRmxaRUQ5a042YkxjY3lSLWpTVmlkVXlNeEl5VXN3VUE0NTR3NUV4WlpVUElGTnFpUUJJNGVOOVR5Njd5b3dKY3hENGlRVmQ4dkhCbDU1UWEtQ2pEU3g4aGVHcjlhamNvUHNtUkJBTWszZXF1T3FKeWN6MmhxZktvakoxc2dIVTIyQzJKWHo4SHZON3JoSlh4VVpoZl9oRldKT090VGJGV29FeWZfZHY0d2d4bV9mMk5BdXhNb1ZMVHg0STZnMU13clEwU1hfdWFVZkJfMmMyVnB3dV9PM3YxNmtTUy16aGIuQWlVRFlJSk80RGxrNlFSTlRiMmU4dw.Ac40dogte6hp0drz19IfxQy6Fln3mNAeaHkidVpl0Pv8zBn7vsXHchJKHkU_5rcwPaxY70DxD1NQOQ3iROdWDg00ekbzqFt7nGRuBFiWQc2fenBGfSctDglqEsQkD6WKVCTxwVm-K4SJBZ7MQfz3_wsslwWzgAH0wloIw6NRYc4GhW5lxdF3l7fFpOokxlTZT3O8U3UCZ6BTsTCNmLeEaPgRtCMxtbK67Lk3FXhjwVU0BlwgFrqq2LWmQ19Bh7xYOWn3gPVB4zX02S-FCy_tfcGUAQvLv6JwBcPngxKh_SYPFwV-2vSwkOpVp0aHlerVj0EJEVw1H6puGsX9kdi5xg";
        File file = new File("C:\\etc\\cas\\config\\keystore.jwks");

        String jwks = Files.readLines(file, Charsets.UTF_8).stream().collect(Collectors.joining(System.lineSeparator()));
        JsonWebKeySet jsonWebKeySet = new  JsonWebKeySet(jwks);
        List<JsonWebKey> keys = jsonWebKeySet.getJsonWebKeys();
        //PublicJsonWebKey rsaKey = RsaJsonWebKey.Factory.newPublicJwk(keys.get(0));
        PublicJsonWebKey rsaKey = RsaJsonWebKey.Factory.newPublicJwk(keys.get(0).getKey());
        //keys.get(0).getKey()

        //FileSystemResource resource = new FileSystemResource("/etc/cas/config/caspublic.key");
        //PublicKeyFactoryBean publicKeyFactoryBean = new PublicKeyFactoryBean();
        //publicKeyFactoryBean.setAlgorithm(RsaKeyUtil.RSA);
        //publicKeyFactoryBean.setResource(resource);
        //publicKeyFactoryBean.setSingleton(false);
        //PublicKey publicKey = publicKeyFactoryBean.getObject();

        //val asString = new String(value, StandardCharsets.UTF_8);
        //return verifyJwsSignature(signingKey, asString);

        val jws = new JsonWebSignature();
        jws.setCompactSerialization(jwt);
        jws.setKey(rsaKey.getPublicKey());

        val verified = jws.verifySignature();
        if (verified) {
            //val payload = jws.getEncodedPayload();
            //LOGGER.trace("Successfully decoded value. Result in Base64url-encoding is [{}]", payload);
            //return EncodingUtils.decodeUrlSafeBase64(payload);
            System.out.println("verified");
            /*FileSystemResource privateResource = new FileSystemResource("/etc/cas/config/casprivate.key");
            PrivateKeyFactoryBean privateKeyFactoryBean = new PrivateKeyFactoryBean();
            privateKeyFactoryBean.setAlgorithm(RsaKeyUtil.RSA);
            privateKeyFactoryBean.setLocation(privateResource);
            privateKeyFactoryBean.setSingleton(false);

            val payload = jws.getEncodedPayload();
            final byte[] decodedBytes = Base64.decodeBase64(payload.getBytes(StandardCharsets.UTF_8));
            final String decodedPayload = new String(decodedBytes, StandardCharsets.UTF_8);

            val jwe = new JsonWebEncryption();
            jwe.setKey(privateKeyFactoryBean.getObject());
            jwe.setCompactSerialization(decodedPayload);
            System.out.println("Decrypting value...");
            try {
                String res = jwe.getPayload();
                System.out.println(res);
            } catch (final Exception e) {
               e.printStackTrace();
            }*/
            //String encryptionKey = "osFdnwrVA3lIoARpUX6CFGjspjjkFmE0Cze7bVZTfio";


            final byte[] decodedBytes = Base64.decodeBase64(jws.getEncodedPayload().getBytes(StandardCharsets.UTF_8));
            final String decodedPayload = new String(decodedBytes, StandardCharsets.UTF_8);
            System.out.println(decodedPayload);
            JwtClaims claims = JwtClaims.parse(decodedPayload);
            Date date = new Date(claims.getExpirationTime().getValue() * 1000);
            System.out.println("expired date: " + date.toLocaleString());
            //final JsonWebEncryption jwe = new JsonWebEncryption();
            /*final JsonWebKey jsonWebKey = JsonWebKey.Factory
                    .newJwk("\n" + "{\"kty\":\"oct\",\n" + " \"k\":\"" + encryptionKey + "\"\n" + "}");*/

            //jwe.setCompactSerialization(decodedPayload);
            //jwe.setKey(new AesKey(jsonWebKey.getKey().getEncoded()));
            //jwe.setKey(rsaKey.getPublicKey());
            //System.out.println(jwe.getPlaintextString());
        } else {
            System.out.println("can't verify");
        }
    }
}
