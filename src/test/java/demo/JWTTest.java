package demo;

import lombok.val;
import org.apache.commons.codec.binary.Base64;
import org.jose4j.jwe.ContentEncryptionAlgorithmIdentifiers;
import org.jose4j.jwe.JsonWebEncryption;
import org.jose4j.jwe.KeyManagementAlgorithmIdentifiers;
import org.jose4j.jwk.JsonWebKey;
import org.jose4j.jws.JsonWebSignature;
import org.jose4j.keys.AesKey;
import org.jose4j.keys.RsaKeyUtil;
import org.junit.Test;
import org.springframework.core.io.FileSystemResource;

import java.nio.charset.StandardCharsets;
import java.security.Key;
import java.security.PublicKey;

public class JWTTest {

    @Test
    public void encode() throws Exception {
        final String signingKey = "FSGeyZDp7VgPWJKVAS7sF-FIbfQ0822wkQuF48yrT1VlBTI290scULQr9RB2Zd0UJZuf-T061saJXokqvDROOg";
        final String encryptionKey = "osFdnwrVA3lIoARpUX6CFGjspjjkFmE0Cze7bVZTfio";

        final JsonWebKey jsonWebKey = JsonWebKey.Factory
                .newJwk("\n" + "{\"kty\":\"oct\",\n" + " \"k\":\"" + encryptionKey + "\"\n" + "}");
        final Key eKey = new AesKey(jsonWebKey.getKey().getEncoded());
        final Key signkey = new AesKey(signingKey.getBytes(StandardCharsets.UTF_8));

        val encoded = EncodingUtils.encryptValueAsJwt(eKey, "value", KeyManagementAlgorithmIdentifiers.DIRECT, ContentEncryptionAlgorithmIdentifiers.AES_128_CBC_HMAC_SHA_256);
        //val encoded = EncodingUtils.encryptValueAsJwtDirectAes128Sha256(signkey, "value", KeyManagementAlgorithmIdentifiers.DIRECT);
        System.out.println("encoded:" + encoded);
        val signed = EncodingUtils.signJwsHMACSha512(signkey, encoded.getBytes(StandardCharsets.UTF_8));
        System.out.println("ss:" +  new String(signed, StandardCharsets.UTF_8));
    }

    @Test
    public void rsaKeyTest() throws Exception {
       String jwt = "eyJhbGciOiJSUzUxMiIsInR5cCI6IkpXVCJ9.ZXlKNmFYQWlPaUpFUlVZaUxDSmhiR2NpT2lKa2FYSWlMQ0psYm1NaU9pSkJNVEk0UTBKRExVaFRNalUySWl3aVkzUjVJam9pU2xkVUlpd2lkSGx3SWpvaVNsZFVJbjAuLnFrY0ZyUXpzNHdyZTZxZmI5Q3hqWHcudldrVkFWc0NHMktxdjdGdlR5dlQzV000Z2FxU2F4VVdKVDVwTmN2T19VYmh2cFkyYUVfSkQwVzVRUmhncnVYQ1cxblQ0MXZWSXFDX3llbmx1NFlXbjRiNGhQMkpQT2NiRmxaRUQ5a042YkxjY3lSLWpTVmlkVXlNeEl5VXN3VUE0NTR3NUV4WlpVUElGTnFpUUJJNGVOOVR5Njd5b3dKY3hENGlRVmQ4dkhCbDU1UWEtQ2pEU3g4aGVHcjlhamNvUHNtUkJBTWszZXF1T3FKeWN6MmhxZktvakoxc2dIVTIyQzJKWHo4SHZON3JoSlh4VVpoZl9oRldKT090VGJGV29FeWZfZHY0d2d4bV9mMk5BdXhNb1ZMVHg0STZnMU13clEwU1hfdWFVZkJfMmMyVnB3dV9PM3YxNmtTUy16aGIuQWlVRFlJSk80RGxrNlFSTlRiMmU4dw.Ac40dogte6hp0drz19IfxQy6Fln3mNAeaHkidVpl0Pv8zBn7vsXHchJKHkU_5rcwPaxY70DxD1NQOQ3iROdWDg00ekbzqFt7nGRuBFiWQc2fenBGfSctDglqEsQkD6WKVCTxwVm-K4SJBZ7MQfz3_wsslwWzgAH0wloIw6NRYc4GhW5lxdF3l7fFpOokxlTZT3O8U3UCZ6BTsTCNmLeEaPgRtCMxtbK67Lk3FXhjwVU0BlwgFrqq2LWmQ19Bh7xYOWn3gPVB4zX02S-FCy_tfcGUAQvLv6JwBcPngxKh_SYPFwV-2vSwkOpVp0aHlerVj0EJEVw1H6puGsX9kdi5xg";
       FileSystemResource resource = new FileSystemResource("/etc/cas/config/caspublic.key");
        PublicKeyFactoryBean publicKeyFactoryBean = new PublicKeyFactoryBean();
        publicKeyFactoryBean.setAlgorithm(RsaKeyUtil.RSA);
        publicKeyFactoryBean.setResource(resource);
        publicKeyFactoryBean.setSingleton(false);
        PublicKey publicKey = publicKeyFactoryBean.getObject();

        //val asString = new String(value, StandardCharsets.UTF_8);
        //return verifyJwsSignature(signingKey, asString);

        val jws = new JsonWebSignature();
        jws.setCompactSerialization(jwt);
        jws.setKey(publicKey);

        val verified = jws.verifySignature();
        if (verified) {
            //val payload = jws.getEncodedPayload();
            //LOGGER.trace("Successfully decoded value. Result in Base64url-encoding is [{}]", payload);
            //return EncodingUtils.decodeUrlSafeBase64(payload);
            System.out.println("verified");
            /*FileSystemResource privateResource = new FileSystemResource("/etc/cas/config/casprivate.key");
            PrivateKeyFactoryBean privateKeyFactoryBean = new PrivateKeyFactoryBean();
            privateKeyFactoryBean.setAlgorithm(RsaKeyUtil.RSA);
            privateKeyFactoryBean.setLocation(privateResource);
            privateKeyFactoryBean.setSingleton(false);

            val payload = jws.getEncodedPayload();
            final byte[] decodedBytes = Base64.decodeBase64(payload.getBytes(StandardCharsets.UTF_8));
            final String decodedPayload = new String(decodedBytes, StandardCharsets.UTF_8);

            val jwe = new JsonWebEncryption();
            jwe.setKey(privateKeyFactoryBean.getObject());
            jwe.setCompactSerialization(decodedPayload);
            System.out.println("Decrypting value...");
            try {
                String res = jwe.getPayload();
                System.out.println(res);
            } catch (final Exception e) {
               e.printStackTrace();
            }*/
            String encryptionKey = "osFdnwrVA3lIoARpUX6CFGjspjjkFmE0Cze7bVZTfio";

            final byte[] decodedBytes = Base64.decodeBase64(jws.getEncodedPayload().getBytes(StandardCharsets.UTF_8));
            final String decodedPayload = new String(decodedBytes, StandardCharsets.UTF_8);

            final JsonWebEncryption jwe = new JsonWebEncryption();
            final JsonWebKey jsonWebKey = JsonWebKey.Factory
                    .newJwk("\n" + "{\"kty\":\"oct\",\n" + " \"k\":\"" + encryptionKey + "\"\n" + "}");

            jwe.setCompactSerialization(decodedPayload);
            jwe.setKey(new AesKey(jsonWebKey.getKey().getEncoded()));
            System.out.println(jwe.getPlaintextString());
        } else {
            System.out.println("can't verify");
        }

    }

    public enum Action {
        CREATE,
        UPDATE;

        /*private String val;

        Action(String val) {
            this.val = val;
        }

        public String getVal() {
            return val;
        }*/
    }

    @Test
    public void actionTest() {
        System.out.println(Action.valueOf("CREATE1"));
    }

    @Test
    public void eqaulTest() {
        String reqType = "email1";
        if (!(reqType.equals("email") || reqType.equals("phone"))) {
            System.out.println("helloworld");
        } else {
            System.out.println("not found");
        }
    }
}
