package demo;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.springframework.beans.factory.config.AbstractFactoryBean;
import org.springframework.core.io.Resource;

import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.Security;
import java.security.spec.PKCS8EncodedKeySpec;

/**
 * Factory Bean for creating a private key from a file.
 *
 * @author Scott Battaglia
 * @since 3.1
 */
@Slf4j
@Getter
@Setter
public class PrivateKeyFactoryBean extends AbstractFactoryBean<PrivateKey> {

    static {
        Security.addProvider(new BouncyCastleProvider());
    }

    private Resource location;

    private String algorithm;

    @Override
    protected PrivateKey createInstance() {
        PrivateKey key = readDERPrivateKey();
        return key;
    }


    private PrivateKey readDERPrivateKey() {
        System.out.println(String.format("Attempting to read key as DER [{}]", this.location));
        try (val privKey = this.location.getInputStream()) {
            val bytes = new byte[(int) this.location.contentLength()];
            privKey.read(bytes);
            val privSpec = new PKCS8EncodedKeySpec(bytes);
            val factory = KeyFactory.getInstance(this.algorithm);
            return factory.generatePrivate(privSpec);
        } catch (final Exception e) {
          System.out.println(String.format("Unable to read key", e));
            return null;
        }
    }

    @Override
    public Class getObjectType() {
        return PrivateKey.class;
    }

}
