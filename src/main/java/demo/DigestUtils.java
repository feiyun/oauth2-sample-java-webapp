package demo;

import org.apache.commons.codec.binary.Hex;
import org.apache.commons.codec.digest.MessageDigestAlgorithms;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;

public class DigestUtils {
    private static final int ABBREVIATE_MAX_WIDTH = 125;

    /**
     * Computes hex encoded SHA512 digest.
     *
     * @param data data to be hashed
     * @return sha-512 hash
     */
    public static String sha512(final String data) {
        return digest(MessageDigestAlgorithms.SHA_512, data.getBytes(StandardCharsets.UTF_8));
    }

    /**
     * Computes hex encoded SHA256 digest.
     *
     * @param data data to be hashed
     * @return sha-256 hash
     */
    public static String sha256_1(final String data) {
        return digest(MessageDigestAlgorithms.SHA_256, data.getBytes(StandardCharsets.UTF_8));
    }
    public static String sha256(final String data) {
        byte[] bytes = data.getBytes(StandardCharsets.UTF_8);
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            md.update(bytes, 0, bytes.length);
            byte[] digest = md.digest();
            char[] result = Hex.encodeHex(digest);
            return new String(result);
        } catch (final Exception cause) {
            throw new SecurityException(cause);
        }
    }
    /**
     * Computes hex encoded SHA digest.
     *
     * @param data data to be hashed
     * @return sha hash
     */
    public static String sha(final String data) {
        return digest(MessageDigestAlgorithms.SHA_1, data);
    }

    /**
     * Computes SHA digest.
     *
     * @param data data to be hashed
     * @return sha hash
     */
    public static byte[] sha(final byte[] data) {
        return rawDigest(MessageDigestAlgorithms.SHA_1, data);
    }

    /**
     * Computes hex encoded digest.
     *
     * @param alg  Digest algorithm to use
     * @param data data to be hashed
     * @return hex encoded hash
     */
    public static String digest(final String alg, final String data) {
        return digest(alg, data.getBytes(StandardCharsets.UTF_8));
    }

    /**
     * Computes hex encoded digest.
     *
     * @param alg  Digest algorithm to use
     * @param data data to be hashed
     * @return hex encoded hash
     */
    public static String digest(final String alg, final byte[] data) {
        return hexEncode(rawDigest(alg, data));
    }

    public static String hexEncode(final byte[] data) {
        try {
            char[] result = Hex.encodeHex(data);
            return new String(result);
        } catch (final Exception e) {
            return null;
        }
    }

    /**
     * Computes digest.
     *
     * @param alg  Digest algorithm to use
     * @param data data to be hashed
     * @return hash
     */
    public static byte[] rawDigest(final String alg, final byte[] data) {
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            md.update(data, 0, data.length);
            byte[] digest = md.digest();
            return digest;
        } catch (final Exception cause) {
            throw new SecurityException(cause);
        }
    }
}